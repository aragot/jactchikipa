$(setTimeout(load, 0));

  $('#refresh').click(function(e){
    var originalText = $(this).text();
    var link = $(this);
    e.preventDefault();
    link.text('refreshing...');

    load();

    setTimeout(function(){
      link.text(originalText);
    }, 1000);
  })

function clearLocalStorage() { //dev
  localStorage.removeItem("username");
  localStorage.removeItem("stashBaseUrl");
  localStorage.removeItem("stashRepository");
  localStorage.removeItem("bambooBaseUrl");
  localStorage.removeItem("bambooBuilds");
  localStorage.removeItem("JIRAPanels");
}

function load() {

  //clearLocalStorage(); // reset localStorage (dev)

  //TODO: allow partial configuration (i.e: the user has jira server but not bamboo)
  if (!localStorage["username"] || !localStorage["JIRAPanels"] ||
    !localStorage["stashBaseUrl"] || !localStorage["stashProject"] ||
    !localStorage["stashRepository"] || !localStorage["bambooBaseUrl"] ||
    !localStorage["bambooBuilds"]

    ){

    $('#panels').hide();
    $('#globalMsgHolder').show();
    AJS.messages.warning('#globalMsgHolder', { title: 'Configuration missing', body: 'Please <a href="options.html" target="_blank" title="Configuration">click here</a> to create one' });
    return;
  }

  $('#panels').show();
  $('#globalMsgHolder').hide();

  // ---------------------------
  // Stash REST API
  // ---------------------------
  Productivity.Stash.currentUserName = localStorage["username"];
  Productivity.Stash.stashBaseUrl = localStorage["stashBaseUrl"];// 'https://stash.atlassian.com';
  Productivity.Stash.Project = localStorage["stashProject"];
  Productivity.Stash.Repository = localStorage["stashRepository"];

  Productivity.Stash.load();


  // ---------------------------
  // JIRA Dashboard
  // ---------------------------
  Productivity.JIRA.currentUserName = Productivity.Stash.currentUserName; //TODO: needs to be changed
  Productivity.JIRA.JIRAPanels = localStorage["JIRAPanels"];// 'https://jira.atlassian.com';

  Productivity.JIRA.load();


  // ---------------------------
  // Bamboo Dashboard
  // ---------------------------
  Productivity.Bamboo.bambooBaseUrl = localStorage["bambooBaseUrl"];// 'https://jira.atlassian.com';
  Productivity.Bamboo.bambooBuilds = localStorage["bambooBuilds"];// 'https://jira.atlassian.com';
  
  Productivity.Bamboo.load();

};