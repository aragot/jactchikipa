$(function(){

  Productivity = window.Productivity || {};
  Productivity.JIRA = Productivity.JIRA || {};

  var JIRAPlaceHolder;

 /**
  * Create new tab
  * - Returns safe tab id so it can be used later
  */
  function createTab(name) {
    var safeNameID = name.replace(/[^a-zA-Z0-9]+/g, ""); // make sure we create a valid HTML id

    if ($('#tab-jira-tab-' + safeNameID).length==0){ // it does not exists. create tab element
      var strClassTab = "", strClassPane = "";

      if ($('#jira .active-pane').length==0) { // there is not and active panel. set this one as active
        strClassTab = "active-tab";
        strClassPane = "active-pane";
      }

      var li=$('<li class="menu-item ' + strClassTab + '""><a href="#tab-jira-tab-' + safeNameID + '"><strong>' + name + '</strong> (<span id="counterJiraIssues' + safeNameID + '"></span>)</a></li>');

      $('.horizontal-tabs ul.tabs-menu', JIRAPlaceHolder).append(li);

      var divContent=$('<div id="tab-jira-tab-' + safeNameID + '" class="tabs-pane ' + strClassPane + '"><div id="tab-jira' + safeNameID + '-content" class="content"><span class="loading">loading...</span></div>')
      $('.horizontal-tabs', JIRAPlaceHolder).append(divContent);
    }
    return safeNameID;
  }

 /**
  * Populates tab with jira issues.
  *  - If tab does not exist, it will be created.
  */
  function populateTab(safeNameID, items) {
    // pupulate  counter
    $('#counterJiraIssues' + safeNameID).html(items.length);
    // populate issues
    render($('#tab-jira' + safeNameID + '-content'), items);
  }


 /**
  * Render list of issues on the provider placeholder
  */
  function render(placeholder, items){
    var strOutput = ''; //Chrome extension doesn't allow underscore templates
    for (var i = 0, l=items.length; i < l; i++) {
      var item = items[i];

      strOutput += '<div class="issue">';

      strOutput += '<span class="priority">';
      if (item.fields.priority){
        strOutput += '<img alt="' + item.fields.priority.name + '" src="'+ 
          item.fields.priority.iconUrl + '" title="'+  item.fields.priority.name + '">';
      }
      strOutput += '</span>';

      strOutput += '<span class="status">';
      if (item.fields.status){
          strOutput += ' <span class="aui-lozenge aui-lozenge-current aui-lozenge-subtle">';
          strOutput += item.fields.status.name;
          strOutput += '</span>';
      }
      strOutput += '</span>';

      strOutput += '<span class="issuetype">';
      if (item.fields.issuetype){
        strOutput += '<img alt="' + item.fields.issuetype.description + '" src="'+ 
          item.fields.issuetype.iconUrl + '" title="'+  item.fields.issuetype.description + '">';
      }
      strOutput += '</span>';

      strOutput += '<span class="components">';
      if (item.fields.components){
        for (var c = item.fields.components.length - 1; c >= 0; c--) {
          var comp = item.fields.components[c];
          strOutput += ' <span class="aui-lozenge aui-lozenge-subtle">';
          strOutput += comp.name;
          strOutput += '</span>';
        };
      }
      strOutput += '</span>';

      strOutput += ' <span class="title">';
      var url = item.self.split('/rest/api')[0] + '/browse/' + item.key;
      strOutput += '<a href="' + url + '" target="_blank" title="'+ item.fields.summary + '">';
      strOutput += item.fields.summary;
      strOutput += '</a>';
      strOutput += '</span>';
      strOutput += '</div>';
    };

    placeholder.html(strOutput);
  }


  Productivity.JIRA.showError = function (title, message){
    $('#jiraerror').empty();
    AJS.messages.error('#jiraerror', { title: title, body: message });
    JIRAPlaceHolder.hide();
  }


  Productivity.JIRA.load = function (){
    JIRAPlaceHolder = $('#jira');
    var maxResults = 10;
    var items = Productivity.JIRA.JIRAPanels.split('\n');

    // convert to objects
    items = items.map(function(itemStr){
      return {
        name : itemStr.split('|')[0],
        url: itemStr.split('|')[1]
      };
    });

    // make sure are valid
    items =_.reject(items, function(item){
      return !item.name || !item.url;
    });

    $.each(items, function(index, item){
      item.safeNameID = createTab(item.name);
    });

    $.each(items, function(index, item){

      (function(id, url){

        var cacheKey = 'jira-issues-asigned-to-me' + url;
        $.ajax({
          url : url,
          success : function(data){
            populateTab(id, data.issues);

            Productivity.Cache.set(cacheKey, data.issues);
            Productivity.Helpers.queueNotification('jira issues updated..');

            JIRAPlaceHolder.show();
          },
          error: function(err){
            Productivity.Cache.remove(cacheKey);
            if (err.status==401 || err.status==403){
              Productivity.JIRA.showError('Unauthorized', 'Please authenticate with the JIRA server to open a session');
            }
            else if (err.status==404){
              Productivity.JIRA.showError('Not found', 'Please check the url ' + item.url);
            }
            else {
              Productivity.JIRA.showError('Error loading JIRA issues', JSON.stringify(err));
              console.log(item);
              console.error(err);
            }
          }
        });

        // while the ajax call is being processed, we can show stuff from the cache, if we have.
        var itemscached = Productivity.Cache.get(cacheKey);
        if (itemscached){
          console.log('from cache');
          JIRAPlaceHolder.show();
          populateTab(id, itemscached);
        }

      }(item.safeNameID, item.url));
    });
  }
});