// Saves options to localStorage.
function save_options() {
  localStorage["username"] = document.getElementById("username").value;
  localStorage["JIRAPanels"] = document.getElementById("JIRAPanels").value;
  localStorage["stashBaseUrl"] = document.getElementById("stashBaseUrl").value;
  localStorage["stashProject"] = document.getElementById("stashProject").value;
  localStorage["stashRepository"] = document.getElementById("stashRepository").value;
  localStorage["bambooBuilds"] = document.getElementById("bambooBuilds").value;
  localStorage["bambooBaseUrl"] = document.getElementById("bambooBaseUrl").value;

  printmsg('Options Saved.');
}

function printmsg(msg){
  // Update status to let user know options were saved.
  AJS.$('#okmsg').show();
  setTimeout(function() {
    AJS.$('#okmsg').fadeOut();
  }, 1000);
}

// Restores select box state to saved value from localStorage.
function restore_options() {
  var DEFAULT_BAMBOO_BUILDS = ['CONFFUNC-MAIN',
            'CONFFUNC-WD',
            'CONFFUNC-COMPAT',
            'COD-MODAT',
            'COD-MSATP',
            'COD-MSSTP',
            'COD-MAATP',
            'CONFGEN-SELENIUMFF',
            'CONFUI-QUNITCHROMELATEST',
            'CONFUI-QUNITFFLATEST',
            'CONFUI-QUNITCHROMEPLUGINS',
            'CONFUI-QUNITQCFFLATEST',
            'CONFFUNC2-LICENSECHK'].join(', ');
  document.getElementById("username").value = localStorage["username"] || '';
  document.getElementById("JIRAPanels").value = localStorage["JIRAPanels"] || 
    'Open|https://jira.atlassian.com/rest/api/2/search?jql=assignee%20%3D%20currentUser()%20AND%20status%20%3D%20OPEN&maxResults=20' +
    '\n' +
    'Latest 10 issues I reported|https://jira.atlassian.com/rest/api/2/search?jql=reporter%3DcurrentUser()%20ORDER%20BY%20priority&maxResults=10' +
    '\n' + 
    'My issues on TR or QA|https://jira.atlassian.com/rest/api/2/search?jql=assignee%3D%20currentUser()%20and%20status%20in%20(%22Technical%20Review%22%2C%20%22Quality%20Review%22)%20ORDER%20BY%20priority&maxResults=20';

  document.getElementById("stashBaseUrl").value = localStorage["stashBaseUrl"] || 'https://stash.atlassian.com';
  document.getElementById("stashProject").value = localStorage["stashProject"] || 'CONF';
  document.getElementById("stashRepository").value = localStorage["stashRepository"] || 'confluence';
  document.getElementById("bambooBuilds").value = localStorage["bambooBuilds"] || DEFAULT_BAMBOO_BUILDS;
  document.getElementById("bambooBaseUrl").value = localStorage["bambooBaseUrl"] || 'https://confluence-bamboo.internal.atlassian.com';
}

document.addEventListener('DOMContentLoaded', restore_options);
document.querySelector('#save').addEventListener('click', save_options);