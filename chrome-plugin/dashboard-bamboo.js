$(function(){

  Productivity = window.Productivity || {};
  Productivity.Bamboo = Productivity.Bamboo||{};

  function renderBuild(placeholder, name, build){
    var buildBrowseUrl = Productivity.Bamboo.bambooBaseUrl + '/browse/';
    //Chrome extension doesn't allow underscore templates
    var strRow = '<tr id="build-' + build.key + '">'; 
    strRow +='<td>';
    var css = (build.state=='Failed') ? "error" : "success";
    strRow +='<span class="build-status aui-lozenge aui-lozenge-' + css + '">';
    strRow +='<a target="_blank" href="' + buildBrowseUrl + build.key+ '">';
    strRow +=build.planName;
    strRow +='</a>';
    strRow +='</span>';
    strRow +='</td>';

    strRow +='<td>';
    strRow +='<span title="Number of consecutive failed builds" class="aui-lozenge aui-lozenge-error aui-lozenge-subtle">';
    strRow +=build.failedTestCount;
    strRow +='<span>';
    strRow +='</td>';

    strRow +='<td class="build-duration">';
    strRow +='<span title="Build duration">took ';
    strRow +=build.buildDurationDescription;
    strRow +='</span>';
    strRow +='</td>';

    strRow +='<td class="build-timeago">';
    strRow +='<span>';
    strRow +=build.buildRelativeTime;
    strRow +='</span>';
    strRow +='</td>';

    strRow +='</tr>';
    $('#build-' + name).replaceWith(strRow);
  }

  Productivity.Bamboo.load = function (){
    var bambooPlaceHolder = $('#bamboo');
    var bambooMyBuildsPlaceHolderTBody = $('#tab-bamboo-mybuilds tbody', bambooPlaceHolder);

    var builds = Productivity.Bamboo.bambooBuilds.replace(/\s/g, '').split(',');

    bambooMyBuildsPlaceHolderTBody.empty();

    $('#bambooCounterMyBuilds').html(builds.length);
    $.each(builds, function(index, item){
      bambooMyBuildsPlaceHolderTBody.append('<tr id="build-'+ item +'"><td colspan=5><span class="loading">loading...</span></td></tr>');
    });

    $.each(builds, function(index, item){
      var cacheKey = 'bamboo-builds' + item;
      $.ajax({

        url : Productivity.Bamboo.bambooBaseUrl + '/rest/api/latest/result/' + item + '-latest.json',

        success : function(data){
          renderBuild(bambooMyBuildsPlaceHolderTBody, item, data);
          Productivity.Helpers.queueNotification('bamboo build ' + item + ' updated..');
        },

        error: function(err){
          if (err.status==401 || err.status==403){
            errMsg = item + ' unauthorized. Please authenticate with the Bamboo server <a href="' + Productivity.Bamboo.bambooBaseUrl + '" target="_blank">here</a> to open a session';
          }
          else if (err.status==404){
            errMsg = item + ' not found.';
          }
          else if (err.status==500){
            errMsg = item + ' bamboo server is unavailable. Error 500';
          }
          else {
            errMsg = item + '. Unknown error loading build' ;
            console.error(err);
          }

          $('#build-' + item).replaceWith('<tr id="build-'+ item +'"><td colspan=5><span class="error">' + errMsg + '</span></td></tr>');
        }
      });

    });
  };
});