$(function(){
  Productivity = window.Productivity || {};
  Productivity.Stash = Productivity.Stash || {};

 /**
  * Render helpers
  */
  function renderAvatar (user){
    return '<img alt="' + user.name + '" class="avatar small" src="https://secure.gravatar.com/avatar/' + md5(user.emailAddress) + '" title="' + user.name + '">';
  }

  function render(placeholder, items){
    var strOutput = ''; //Chrome extension doesn't allow underscore templates
    strOutput += '<table class="pullRequest">';
    strOutput += '<tbody>';
    for (var i = items.length - 1; i >= 0; i--) {
      var item = items[i];
      strOutput += '<tr>';
      
      strOutput += '<td>';
      strOutput += '<span class="title">';
      strOutput += '<a href="' + Productivity.Stash.stashBaseUrl + item.link.url + '" target="_blank" title="'+ item.title + '">';
      strOutput += item.title;
      strOutput += '</a>';
      strOutput += '</span>';
      strOutput += '</td>';
      
      strOutput += '<td>';
      strOutput += '<span class="description">';
      strOutput += item.description;
      strOutput += '</span>';
      strOutput += '</td>';
      
      strOutput += '<td class="reviewers approved">';
      for (var r = item.reviewers.length - 1; r >= 0; r--) {
        var rev = item.reviewers[r];
        if (rev.approved){
          strOutput += renderAvatar(rev.user);
        }
      };
      strOutput += '</td>';
      
      strOutput += '<td class="reviewers notapproved">';
      for (var r = item.reviewers.length - 1; r >= 0; r--) {
        var rev = item.reviewers[r];
        if (!rev.approved){
          strOutput += renderAvatar(rev.user);
        }
      };
      strOutput += '</td>';
      
      strOutput += '</tr>';
    };
    strOutput += '</tbody>';
    strOutput += '</table>';

    placeholder.html(strOutput);
  }

 /**
  * Bolean helpers
  */
  function isAuthor (item) {
    return item.author.user.name == Productivity.Stash.currentUserName;
  }

  function isReviewer (item) {
    return _.find(item.reviewers, function(reviewer) {
      return reviewer.user.name == Productivity.Stash.currentUserName;
    });
  }

  function isReviewerAndApproved (item) {
    return _.find(item.reviewers, function(reviewer) {
      return (reviewer.approved && reviewer.user.name == Productivity.Stash.currentUserName);
    });
  }

  function isReviewerAndHasNotApproved (item) {
    return _.find(item.reviewers, function(reviewer) {
      return (!reviewer.approved && reviewer.user.name == Productivity.Stash.currentUserName);
    });
  }

 /**
  * Filters
  */
  function filterByAuthorName (items, name){
    return _(items).filter(isAuthor);
  }

  function filterByContainsReviewer (items, name){
    return _(items).filter(isReviewer);
  }

  function filterByContainsReviewerAndApproved (items, name){
    return _(items).filter(isReviewerAndApproved);
  }

  function filterByContainsReviewerAndHasNotApproved (items, name){
    return _(items).filter(isReviewerAndHasNotApproved);
  }

 /**
  * Pull requests pending for me to review them
  */
  function displayPendingForMeToReview(items) {
    items = filterByContainsReviewerAndHasNotApproved(items, Productivity.Stash.currentUserName);

    $('#counterPullRequestToReview').html(items.length);
    render($('#stash_to_review'), items);
  }

  Productivity.Stash.showError = function (title, message){
    $('#stasherror').empty();
    AJS.messages.error('#stasherror', { title: title, body: message });
    stashPlaceHolder.hide();
  }

 /**
  * My pull requests waiting for other people to review them
  */
  function displayWaitingForOthersToReviewThem(items) {
    var myPRItems = filterByAuthorName (items, Productivity.Stash.currentUserName);

    $('#counterPullRequestWaiting').html(myPRItems.length);
    render($('#stash_waiting'), myPRItems);
  }

  var stashPlaceHolder;
  Productivity.Stash.load = function(){
    stashPlaceHolder = $('#stash');
    var urlPullRequest  = Productivity.Stash.stashBaseUrl + "/rest/api/1.0/projects/" + Productivity.Stash.Project +
      "/repos/" + Productivity.Stash.Repository + "/pull-requests?state=OPEN&order=NEWEST";

    var cacheKey = 'stash-' + urlPullRequest;
    $.
    ajax({
      url : urlPullRequest,
      success : function(data){
        stashPlaceHolder.show();
        displayPendingForMeToReview(data.values);
        displayWaitingForOthersToReviewThem(data.values);

        Productivity.Cache.set(cacheKey, data.values);
        Productivity.Helpers.queueNotification('stash data updated..');

      },
      error: function(err){
        Productivity.Cache.remove(cacheKey);
        if (err.status==401 || err.status==403){
          Productivity.Stash.showError('Unauthorized', 'Please authenticate with the Stash server <a href="' + Productivity.Stash.stashBaseUrl + '" target="_blank">here</a> to open a session');
        }
        else{
          Productivity.Stash.showError('Error loading stash PR', JSON.stringify(err));
          console.log(urlPullRequest);
          console.error(err);
        }
      }
    });

    var itemscached = Productivity.Cache.get(cacheKey);
    if (itemscached){
      displayPendingForMeToReview(itemscached);
      displayWaitingForOthersToReviewThem(itemscached);
    }

  };
});