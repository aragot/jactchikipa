$(function(){

  // Since Bamboo has integrated this feature, we don't need this js anymore
  return;


  var link = $('#breadcrumb .current a').attr('href');
  if ($("#build-details").size() == 0 || !link) {
    return; // Not a Bamboo page
  }
  var masterBuild = $('#breadcrumb .branch-selector').hasClass('no-branch-selected'),
      branchName = $('#breadcrumb .branch-selector .plan-branch-name').text(),
      planDescription = $('#build-details .plan-description').data('jira-issue-key'),
      $failedTests = $(".tests-table .failed .test");

  if (!masterBuild && !!branchName) {
    var antiInfiniteLoop = 8;
    $failedTests.each(function(){
      if (antiInfiniteLoop-- < 0) return;
      var $td = $(this),
          // testLink is in the form /browse/CONFFUNC-MAIN3-BATCH6-1/test/case/359662629
          testLink = $td.find('.test-name').attr('href'),
          testClass = $td.find('.test-class').text(),
          testMethod = $td.find('.test-name').text();
          
      if (!testLink && !testClass && !testMethod) { return; }
      var jobFullKey = testLink.split(/\//)[2];
      if (!jobFullKey) { return; }
      var projectKey = jobFullKey.split(/-/)[0],
          planKey = jobFullKey.split(/-/)[1],
          jobKey = jobFullKey.split(/-/)[2],
          numberKey = jobFullKey.split(/-/)[3];
      var jobFullKeyOnMaster = projectKey + '-' + planKey.split(/[0-9]/)[0] + '-' + jobKey + '-latest', // -13508
          buildKeyOnMaster = projectKey + '-' + planKey.split(/[0-9]/)[0] + '/latest',
          url = '/rest/api/latest/result/' + jobFullKeyOnMaster + '?expand=testResults.allTests';
      $.ajax(url).done(function(data) {
        var $data = $(data),
            $testResults = $data.find("testResult[methodName='" + testMethod + "']");
            // Bring the url to the closure, for test purpose:
            ajaxUrl = url;
        $testResults.each(function(){
          var $test = $(this);
          if ($test.attr('className').indexOf(testClass) != 0) {
            var status = $test.attr('status');
            if (status == 'successful') {
              status = 'Successful';
            } else if (status == 'failed') {
              status = 'Failed';
            } else {
              status = '?' + status;
            }
            if ($test.parent().is('quarantinedtests')) {
              status += ' (but quarantined)';
            }
            $('<br/>').appendTo($td);
            $("<a/>").attr('href', '/browse/' + buildKeyOnMaster)
                     .attr('title', 'Brought to you by Productivity for Atlassian Suite')
                     .text(status + ' on the main build').appendTo($td);

            $("<span/>").text(' - ').appendTo($td);
            $("<a/>").attr('href', '#')
                     .attr('title', 'Will like the page on PUG')
                     .attr('class', 'like-adrien-extension')
                     .html('Like?').appendTo($td);
          }
        });
      }).error(function(data, d2, d3){
        debugger;
      });
    });

    // Post usage info
    var likeCount = 0,
        thankYou = ["Thank you!", "Thank you!", "Thank you!", "You like this!",
                    "You really like this!", "Again Again!", "Yuuuhuuu!",
                    "You might have a thing for that button...", "Enough...", "Enough...", "Enough...",
                    "Keep clicking!?", "Ok"];
    $failedTests.on('click', '.like-adrien-extension', function(e) {
      var href = window.location.href.split(/\/browse/)[0] + link;
      e.preventDefault();
      $.ajax({type:'post', url:"https://pug.jira.com/wiki/rest/tinymce/1/content/289374314/comment?actions=true",
        data:{html:"<p>This extension helped me on <a href='" + href + "'>" + href + "</a>. Thank you!</p>", watch:false},
        headers: {'X-Atlassian-Token':'nocheck'}});
      $(this).text(thankYou[likeCount++]);
    });
  }
});