$(function(){
  
  Productivity = window.Productivity || {};
  Productivity.Helpers = Productivity.Helpers || {};

  Productivity.Helpers.queueNotification = function (message, delayHide){
    var notifications = $('#notifications');
    notifications.show();
    var notification = $('<div>' + message + '</div>');
    notification.hide();
    setTimeout(function(){
      notification.remove();
      if (!$('div:visible', notifications).length){
        notifications.hide(); 
      }
    }, 700);
    notifications.append(notification);
    notification.fadeIn();
  };

});