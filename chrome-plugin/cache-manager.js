$(function(){
  Productivity = window.Productivity || {};
  Productivity.Cache = {};

  Productivity.Cache.get = function(key){
    if (localStorage[key]){
      try{
        var storeObj = JSON.parse(localStorage[key]);
        if (storeObj.expiration < +new Date()){
          Productivity.Cache.remove(key);
          return null;
        } else {
          return storeObj.val;
        }
      }
      catch(e){
        console.error('error parsing cache key: ' + key);
        return null;
      }
    }
    else {
      return null;
    }
  };

  Productivity.Cache.set = function(key, value){
    var expiration = 1000 * 60 * 2; 
    var storeObject = {
      expiration : +new Date + expiration,
      val: value
    };
    return localStorage[key] = JSON.stringify(storeObject);
  };

  Productivity.Cache.remove = function(key){
    return localStorage.removeItem(key);
  };

});