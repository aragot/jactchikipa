$(function(){



  if ($("a#project-name-val").size() == 0) {
    return;
  }
  var baseUrl = $("a#project-name-val").attr('href').split("/browse")[0],
      issueKey = window.location.href.split("/browse/")[1].split(/[\/\?]/)[0],
      projectKey = issueKey.split('-')[0],
      fisheyeRepository = 'confluence-git',
      versionNumber = $('meta[name="ajs-version-number"]').attr('content'),
      fisheyeBaseUrl,
      bambooBaseUrl;

  // Check the JIRA version
  if (versionNumber < '5.1') {
    console.log('Productivity for Atlassian Suite - This Chrome extension doesn\'t handle this version: ' + versionNumber);
    return;
  }


  if (window.location.href.indexOf('https://jira.atlassian.com') == 0) {
    //fisheyeBaseUrl = 'https://atlaseye.atlassian.com';
    bambooBaseUrl = 'https://confluence-bamboo.internal.atlassian.com';
  } else {
    $('ul#studio-tabs li a').each(function(){
      var $this = $(this),
          text = $this.text(),
          url = $this.attr('href');
      if (text == 'Reviews') {
        fisheyeBaseUrl = url.split(/\/cru/)[0];
      } else if (text == 'Builds') {
        bambooBaseUrl = url.split(/\/browse/)[0];
      }
    });
  }

  // ====== END OF INTRODUCTION

  // ====== Modifications of the page
  // Change the click on the project name for a query, change the click on the icon for the project home
  var $projectLink = $("a#project-name-val"),
      $projectAvatar = $("img#project-avatar");

    $projectAvatar.wrap("<a>");
    $projectAvatar.parent().attr('href', $projectLink.attr('href'));

  $projectLink
    .attr('href', baseUrl + '/secure/IssueNavigator.jspa?reset=true&jqlQuery=project+%3D+' + projectKey + '+ORDER+BY+updated+DESC');

  // Change the click on a version for a query
  var $versionLinks = $("#fixVersions-field a");
  $versionLinks.each(function(){
    var $link = $(this),
        version = $link.attr('href').split('fixforversion/')[1];

      $link.attr('href', baseUrl + '/secure/IssueNavigator.jspa?reset=true&jqlQuery=project+%3D+' + projectKey + '+AND+fixVersion+%3D+' + version + '+ORDER+BY+updated+DESC');
  });

  // Add a link under 'fix version' to Manage Versions (/plugins/servlet/project-config/ADVMACROS/versions)
  $("<a/>")
    .attr('href', baseUrl + '/plugins/servlet/project-config/' + projectKey + '/versions')
    .text("Manage").before("(").after(")")
    .appendTo("#fixfor-val");



  // Add a section after the Description:
  var $section = $('<div class="module toggle-wrap" id="tweakmyjiramodule">'+
  '<div id="tweakmyjiramodule_heading" class="mod-header">' +
    '<h3 id="productivitymodule_heading" class="toggle-title">Productivity for Atlassian Suite</h3></div><div class="mod-content">'+
    '<div id="description-val">'+
    '<p></p>'+
    '<div class="mod-content">'+
      '<ul id="issuedetails" class="property-list two-cols">'+
        ''+
        '<li class="item">'+
        '<div class="wrap">'+
        '    <strong class="name">Version:</strong>'+
        '    <span id="likes-val" class="value"><span class="placeholder">searching...</span></span>'+
        '</div>'+
        ''+
        '</li>'+
        '<li class="item item-right">'+
        '<div class="wrap" style="padding-left:0;">'+
        '    <strong class="name">Builds:</strong>'+
        '    <span id="builds-val" class="value"><span class="placeholder">searching...</span></span>'+
        '</div>'+
        '</li>'+
        ''+
        '<li class="item">'+
        '<div class="wrap hidden">'+
        '    <strong class="name">Reviews:</strong>'+
        '    <span id="reviews-val" class="value"><span class="placeholder">searching...</span></span>'+
        '</div>'+
        ''+
        '</li>'+
        '<li class="item">'+
        '<div class="wrap hidden">'+
        '    <strong class="name">Branches:</strong>'+
        '    <span id="branches-val" class="value"><span class="placeholder">searching...</span></span>'+
        '</div>'+
        '</li>'+
        ''+
      '</ul>'+
      '</div>'+
    '</div>'+
  '</div>'+
  '</div>' +
// CSS
 '<style>' +
 ' .button-row:hover { background: #eee; } ' +

 '</style>'
);

  $("#descriptionmodule").after($section);




  // Fetch reviews
  if (fisheyeRepository) {
    // Reference: http://docs.atlassian.com/fisheye-crucible/latest/wadl/crucible.html
    var phcb = placeholderCallback("#reviews-val"),
      url = fisheyeBaseUrl + '/rest-service/search-v1/reviews?term=' + issueKey + '&maxReturn=5';
    console.log(url);

    $.ajax({
      url : url,
      success : function(data) {        
        $(data).find('reviews > *').each(function(){
          var $review = $(this),
              id = $review.find('permaid id').text(),
              state = $review.find('state').text(),
              title = $review.find('name').text();

            var $contents = $("<a/>")
              .attr('href', fisheyeBaseUrl + '/cru/' + id)
              .attr('title', title)
              .text(id + " (" + state + ")");

            phcb.add($contents);
        });
        phcb.countDown();
      }
    }).error(phcb.fail(url));
  }

  // Fetch branches
  if (fisheyeBaseUrl) {
    // Reference: http://docs.atlassian.com/fisheye-crucible/latest/wadl/fisheye.html
    var branch1 = 'issue/' + issueKey,
        master1 = 'master',
        branch2 = 'stable-issue/' + issueKey,
        master2 = 'confluence-project-4.3-stable';
        

    function addBranchAndMaster(branch, master, depth, placeholderCallback) {
      var url = fisheyeBaseUrl + '/rest-service-fe/commit-graph-v1/slice/confluence-git?size=' + depth + '&branch=' + branch;
      $.ajax({
        url : url,
        success : function(data) {

          var onBranch = [],
              onMaster = [];
          $(data).find('revisions > *').each(function(){
            var $rev = $(this),
                displayId = $rev.attr('displayId'),
                originalBranch = $rev.attr('branch'),
                $allBranches = $rev.find('branches').text();
            if (originalBranch == branch) {
                if ($allBranches.indexOf(master) != -1) {
                  onMaster.push({displayId: displayId});
                } else {
                  onBranch.push({displayId: displayId});
                }
              }
          });

          if (onMaster.length > 0) {
            placeholderCallback.add(
              $("<a/>").text(master + " (" + onMaster.length + ")")
                .attr('href', fisheyeBaseUrl + '/graph/' + fisheyeRepository + '?csid=' + onMaster[onMaster.length-1].displayId));
          }
          if (onBranch.length > 0) {
            placeholderCallback.add(
              $("<a/>").text(branch + " (" + onBranch.length + " non-merged)")
                .attr('href', fisheyeBaseUrl + '/graph/' + fisheyeRepository + '?csid=' + onBranch[onBranch.length-1].displayId));
          }
            placeholderCallback.countDown();
        }
      }).error(placeholderCallback.fail(url));
    }

    addBranchAndMaster(branch1, master1, 100, placeholderCallback("#branches-val"));
    addBranchAndMaster(branch2, master2, 100, placeholderCallback("#branches-val"));
  }

  if (bambooBaseUrl) {
    // Reference: http://docs.atlassian.com/atlassian-bamboo/REST/4.0/
    var plans = ['CONFFUNC-MAIN',
            'CONFFUNC-WD',
            'CONFFUNC-COMPAT',
            'COD-MODAT',
            'COD-MSATP',
            'COD-MSSTP',
            'COD-MAATP',
            'CONFGEN-SELENIUMFF',
            'CONFUI-QUNITCHROMELATEST',
            'CONFUI-QUNITFFLATEST',
            'CONFUI-QUNITCHROMEPLUGINS',
            'CONFUI-QUNITQCFFLATEST',
            'CONFFUNC2-LICENSECHK'],
        branchName = 'issue-' + issueKey;


    // The REST resource https://confluence-bamboo.internal.atlassian.com/chain/admin/ajax/getChains.action?planKey=CONFUI-SELENIUMFF36BATCH14&_=0
    // returns much more information than the real REST resource.
    // We should prolly use it. But not today.

    // Check the status of the latest build for the branch
    function fetchBuild(plan, placeholderCallback) {
      var url = bambooBaseUrl + '/rest/api/latest/plan/' + plan + '/branch/' + branchName;
   	  var $container = $("<div />", { "class": "button-row" });

      $.ajax({
        url : url,
        success : function(data) {
          if (data != "") {
            var $data = $("branch", data),
                branchKey = $data.attr("key"),
                result = $data.find('latestResult').attr('state'),
                lifeCycleState = $data.find('latestResult').attr('lifeCycleState'),
                $latestCurrentlyActive = $data.find('latestCurrentlyActive'),
                successful, statusImg, tootip,
                name = $data.find('latestResult master').attr('name') || $data.attr('name'),
                link = bambooBaseUrl + '/browse/' + branchKey + '/latest',
                latestCurrentlyActiveCount = $latestCurrentlyActive.attr('number') || 0,
                latestCurrentlyActiveState = $latestCurrentlyActive.attr('lifeCycleState'),
                latestCurrentlyActiveKey = $latestCurrentlyActive.attr('key');

            if (result && result.toLowerCase().indexOf("success") != -1) {
              statusImg = chrome.extension.getURL('plan_successful_16.png');
            } else if (result && result.toLowerCase().indexOf("failed") != -1) {
              statusImg = chrome.extension.getURL('plan_failed_16.png');
            } else if (lifeCycleState && lifeCycleState == 'NotBuilt') {
              statusImg = chrome.extension.getURL('plan_canceled_16.png');
            } else {
              statusImg = chrome.extension.getURL('question_mark.png');
            }
            tootip = 'Lifecycle:' + lifeCycleState + ' - Result:' + result;

			

            var $buildParagraph = $container.append($("<a/>")
                .attr('href', link)
                .attr('title', tootip)
                .attr('data-branch-build', branchKey)
                .text(name)
                .prepend("&nbsp;")
                .prepend('<img src="' + statusImg + '" title="' + tootip + '"/>'));

            //for (var s = 0; s < latestCurrentlyActiveCount; s++) {
            if (latestCurrentlyActiveCount > 0) {
              var runningImg = chrome.extension.getURL('plan_building_16.gif'),
                  runningTooltip = latestCurrentlyActiveState;
              $buildParagraph.prepend('<img src="' + runningImg + '" title="' + runningTooltip + '"/>');
            }

            placeholderCallback.add($buildParagraph);
            placeholderCallback.countDown();

            //fetchRunningBuild(branchKey);

          } else {
            var statusImg = chrome.extension.getURL('question_mark.png'),
                result = 'No data returned in a successful request',
                link = bambooBaseUrl + '/browse/' + plan + '/latest',
				createBuildButton = $('<a class="create-build-button" title="Create a build for this branch" href="#" style="float: right; clear: right;"><img src="' + chrome.extension.getURL('run_16.png') + '"/></a>');
				
			createBuildButton.click(function() {
				var button = $(this);
				$(this).html("Creating…").prop("disabled", true);
				
				var branchKey = $.trim($("meta[name='ajs-issue-key']").attr("content"));
						
				$.ajax({
					url: bambooBaseUrl + "/chain/admin/createPlanBranch.action",
					type: "POST",
					data: {
						creationOption: "AUTO",
						checkBoxFields: "branchesForCreation",
						branchesForCreation: "issue/" + branchKey,
						branchName: "",
						branchDescription: "",
						branchVcsName: "",
						checkBoxFields: "tmp.createAsEnabled",
						planKeyToClone: plan,
						planKey: plan,
						"bamboo.successReturnMode": "json",
						decorator:"nothing",
						confirm:true
					},
					success: function() {
						button.text("Created.");
						fetchBuild(plan, placeholderCallback);
						$container.remove();
						
					},
					error: function() {
						button.text("Failed! Try again?").prop("disabled", false);
					}
					
				});
			});
						
            placeholderCallback.add($container.append(createBuildButton.add($("<a/>")
                .attr('href', link)
                .attr('title', 'No build for this branch?')
                .text(plan)
                .prepend("&nbsp;")
                .prepend('<img src="' + statusImg + '" title="' + result + '"/>')
        				.append("&nbsp;")
                ))
            );
            placeholderCallback.countDown();
          }
        }
      }).error(placeholderCallback.fail(url));
    }

    for (var i = 0 ; i < plans.length ; i++) {
      fetchBuild(plans[i], placeholderCallback("#builds-val"));
    }
  }

  function likes(likesPlaceholderCallback) {
    $.ajax({
      url: chrome.extension.getURL("manifest.json"),
      success: function(data) {
        var manifest = JSON.parse(JSON.minify(data)),
            version = manifest.version;

        $.ajax({
          url: chrome.extension.getURL("manifest-custom.json"),
          success: function(data) {
            var customManifest = JSON.parse(JSON.minify(data)),
                pugTrackerPageId = customManifest.pug_tracker_page_id;
                
            $.post('https://pug.jira.com/wiki/rest/likes/1.0/content/' + pugTrackerPageId + '/likes').success(function(data) {
              likesPlaceholderCallback.add($("<a/>")
                  .text(version + ' - ' + data.likes.length + ' users')
                  .attr('href', 'https://pug.jira.com/wiki/pages/viewpage.action?pageId=' + pugTrackerPageId));
              likesPlaceholderCallback.countDown();
            }).error(likesPlaceholderCallback.fail(url));
          }
        });
      }
    });
  }
  likes(placeholderCallback("#likes-val"));

  function placeholderCallback(placeholder) {
    var $placeholder = $(placeholder),
        nbReq = $placeholder.data('requests') || 0;
    $placeholder.data('requests', ++nbReq);

    function checkEmpty () {
      if (($placeholder.data('requests') || 0) == 0) {
        $placeholder.find("span.placeholder").remove();
      }

		var buttons = $(".create-build-button");
		
		if (buttons.length != 1) {
			return;
		}

	  var createAll = $("<button type='button' class='aui-button aui-button-primary' style='float: right;'>Create all</button>");

	  createAll.click(function() {
	      $(".create-build-button").click();
	      $(this).remove();
	  });

	  $("#builds-val").append(createAll);

    }

    return {
      add : function($elt) {
                $placeholder.prepend($elt);
      },
      countDown : function() {
        var nbReq = $placeholder.data('requests') || 0;
        $placeholder.data('requests', --nbReq);
        this.checkEmpty();
      },
      fail : function(url, loginUrl) {
        return function (xhr){
          var nbReq = $placeholder.data('requests') || 0;
          $placeholder.data('requests', --nbReq);


          var text = xhr && xhr.statusText ? xhr.statusText : "Error",
              loginUrl = loginUrl || (url.length > 8 ? url.substring(0, url.indexOf('/', 8)) : url);

          if (xhr.status == 403 || xhr.status == 401) {
            text = 'Login & Approve';
            url = loginUrl;
          }

          $('<a style="color: red;"/>')
          .text(text)
          .attr('href', url || '#')
          .attr('target', '_blank')
          .attr('title', xhr.responseText)
          .prependTo($placeholder);
          checkEmpty();
        }
      },
      checkEmpty : checkEmpty
    }
  }


  /***************************************************
   * Hard-core **************************************/
  if (false) {
    $("#details-module").insertBefore("#peoplemodule");
    $("#details-module ul#issuedetails").removeClass("two-cols");
    $("#descriptionmodule_heading").remove();
    $("#descriptionmodule")
    .css('margin-top', '100px')
    .css('margin-bottom', '100px');
    /*$(".module")
    .css('margin-top', '50px')
    .css('margin-bottom', '50px');*/
    $("#linkingmodule_heading").remove()
    $("#activitymodule_heading").remove();
    $("#productivitymodule_heading").remove();
  }
});




