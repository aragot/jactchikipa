#!/bin/bash

set -u
set -e

CHROMIUM_BROWSER=/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome
"$CHROMIUM_BROWSER" --pack-extension=chrome-plugin/ --pack-extension-key=chrome-plugin.pem

mv chrome-plugin.crx productivity-for-atlz-suite-chrome-extension.crx

echo "Did you update chrome-plugin/manifest-custom.json with the lastest page to like/track usage ?"
echo "Did you update chrome-plugin/manifest.json ?"
echo "Did you update update-manifest.xml ?"